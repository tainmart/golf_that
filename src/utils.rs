use bevy::{
    prelude::*,
    render::{mesh, mesh::Indices},
};
use bevy_rapier3d::rapier::{dynamics::RigidBodyBuilder, geometry::ColliderBuilder, math::Point};

pub fn load_mesh(
    commands: &mut Commands,
    asset_server: Res<AssetServer>,
    meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    name: &str,
) {
    let asset_handle: Handle<Mesh> = asset_server.get_handle(name);

    let mesh = meshes.get(&asset_handle).unwrap();

    let vertices = match &mesh.attribute(Mesh::ATTRIBUTE_POSITION).unwrap() {
        mesh::VertexAttributeValues::Float3(v) => v
            .iter()
            .map(|p| Point::new(p[0], p[1], p[2]))
            .collect::<Vec<_>>(),
        _ => panic!(),
    };

    let indices = match mesh.indices().unwrap() {
        Indices::U32(i) => Some(i),
        _ => None,
    }
    .unwrap()
    .chunks(3)
    .map(|p| [p[0], p[1], p[2]])
    .collect();

    commands
        .spawn((
            RigidBodyBuilder::new_static(),
            ColliderBuilder::trimesh(vertices, indices),
        ))
        .with_bundle(PbrBundle {
            mesh: asset_handle,
            material: materials.add(Color::rgb(0.5, 0.4, 0.3).into()),
            ..Default::default()
        });
}
