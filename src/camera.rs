use crate::{
    ball::Ball,
    consts::{AppState, STAGE},
};
use bevy::{input::mouse::MouseMotion, prelude::*};
use std::f32::consts::PI;

// TODO: load the following properties from a config file
const MOUSE_SENSITIVITY: f32 = 0.005;
const CAMERA_MOVEMENT_SPEED: f32 = 10.0;

pub struct CameraPlugin;
impl Plugin for CameraPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.init_resource::<InputState>()
            .add_resource(State::new(CameraState::Following))
            .add_stage(CAMERA_STAGE, StateStage::<CameraState>::default())
            .add_system_to_stage(stage::PRE_UPDATE, handle_input.system())
            .on_state_enter(STAGE, AppState::Ingame, spawn_camera.system())
            .on_state_exit(STAGE, AppState::Ingame, despawn_camera.system())
            .on_state_update(
                CAMERA_STAGE,
                CameraState::Following,
                following_camera_movement.system(),
            )
            .on_state_update(
                CAMERA_STAGE,
                CameraState::Freeview,
                freeview_camera_movement.system(),
            );
    }
}

#[derive(Clone)]
enum CameraState {
    Following,
    Freeview,
}

pub struct PlayerCamera {
    yaw: f32,
    pitch: f32,
    zoom: f32,
}

#[derive(Default)]
struct InputState {
    reader_motion: EventReader<MouseMotion>,
}

const CAMERA_STAGE: &str = "camera_stage";

fn toggle_grab_cursor(window: &mut Window) {
    window.set_cursor_position(Vec2::new(window.width() / 2.0, window.height() / 2.0));
    window.set_cursor_lock_mode(!window.cursor_locked());
    window.set_cursor_visibility(!window.cursor_visible());
}

fn spawn_camera(commands: &mut Commands, mut windows: ResMut<Windows>) {
    commands
        .spawn(Camera3dBundle {
            transform: Transform::from_matrix(Mat4::face_toward(
                Vec3::new(1.0, 1.0, 1.0),
                Vec3::new(0.0, 0.0, 0.0),
                Vec3::new(0.0, 1.0, 0.0),
            )),
            ..Default::default()
        })
        .with(PlayerCamera {
            yaw: 0.25 * PI,
            pitch: -0.25 * PI,
            zoom: 10.0,
        });

    toggle_grab_cursor(windows.get_primary_mut().unwrap());
}

fn following_camera_movement(
    mut camera_query: Query<(&PlayerCamera, &mut Transform)>,
    ball_query: Query<(&Ball, &Transform)>,
) {
    for (_, ball_transform) in ball_query.iter() {
        for (camera, mut camera_transform) in camera_query.iter_mut() {
            let ball_pos = ball_transform.translation;
            camera_transform.rotation = Quat::from_rotation_ypr(camera.yaw, camera.pitch, 0.0);
            let direction = camera_transform.forward();
            camera_transform.translation = ball_pos + (direction * camera.zoom);
        }
    }
}

fn freeview_camera_movement(
    mut camera_query: Query<(&PlayerCamera, &mut Transform)>,
    keys: Res<Input<KeyCode>>,
    time: Res<Time>,
) {
    for (camera, mut camera_transform) in camera_query.iter_mut() {
        let mut delta = Vec3::new(0.0, 0.0, 0.0);
        for key in keys.get_pressed() {
            match key {
                KeyCode::W => delta -= Vec3::new(camera.yaw.sin(), 0.0, camera.yaw.cos()),
                KeyCode::S => delta += Vec3::new(camera.yaw.sin(), 0.0, camera.yaw.cos()),
                KeyCode::A => delta -= Vec3::new(camera.yaw.cos(), 0.0, -camera.yaw.sin()),
                KeyCode::D => delta += Vec3::new(camera.yaw.cos(), 0.0, -camera.yaw.sin()),
                KeyCode::Space => delta += Vec3::new(0.0, 1.0, 0.0),
                KeyCode::LControl => delta -= Vec3::new(0.0, 1.0, 0.0),
                _ => (),
            }
        }
        if delta.length() != 0.0 {
            camera_transform.translation +=
                delta.normalize() * CAMERA_MOVEMENT_SPEED * time.delta_seconds();
        }
        camera_transform.rotation = Quat::from_rotation_ypr(camera.yaw, camera.pitch, 0.0);
    }
}

fn handle_input(
    mut camera_state: ResMut<State<CameraState>>,
    keys: Res<Input<KeyCode>>,
    motion: Res<Events<MouseMotion>>,
    mut input_state: ResMut<InputState>,
    mut windows: ResMut<Windows>,
    mut query: Query<&mut PlayerCamera>,
    mut state: ResMut<State<AppState>>,
) {
    if *state.current() != AppState::Ingame {
        return;
    }
    let window = windows.get_primary_mut().unwrap();
    if window.cursor_locked() {
        for mut camera in query.iter_mut() {
            for event in input_state.reader_motion.iter(&motion) {
                camera.pitch -= MOUSE_SENSITIVITY * event.delta.y;
                camera.yaw -= MOUSE_SENSITIVITY * event.delta.x;
            }

            camera.pitch = camera.pitch.clamp(-PI / 2., PI / 2.);
            if camera.yaw > PI {
                camera.yaw -= 2. * PI;
            }
            if camera.yaw < -PI {
                camera.yaw += 2. * PI;
            }
        }
    }

    if keys.just_pressed(KeyCode::Escape) {
        toggle_grab_cursor(windows.get_primary_mut().unwrap());
        state.set_next(AppState::MainMenu).unwrap();
    }

    if keys.just_pressed(KeyCode::V) {
        match camera_state.current() {
            CameraState::Following => camera_state.set_next(CameraState::Freeview).unwrap(),
            CameraState::Freeview => camera_state.set_next(CameraState::Following).unwrap(),
        };
    }
}

fn despawn_camera(commands: &mut Commands, query: Query<Entity, With<Camera3dBundle>>) {
    for entity in query.iter() {
        commands.despawn_recursive(entity);
    }
}
