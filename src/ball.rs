use crate::consts::*;
use bevy::prelude::*;
use bevy_rapier3d::{rapier::dynamics::RigidBodyBuilder, rapier::geometry::ColliderBuilder};

pub struct BallPlugin;
impl Plugin for BallPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.on_state_enter(STAGE, AppState::Ingame, spawn_ball.system())
            .on_state_exit(STAGE, AppState::Ingame, despawn_ball.system());
    }
}

pub struct Ball;
fn spawn_ball(
    commands: &mut Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    commands
        .spawn((
            RigidBodyBuilder::new_dynamic().translation(0.0, 1.0, 0.0),
            ColliderBuilder::ball(0.1),
        ))
        .with_bundle(PbrBundle {
            mesh: meshes.add(Mesh::from(shape::Icosphere {
                radius: 0.1,
                ..Default::default()
            })),
            material: materials.add(Color::rgb(0.0, 1.0, 0.0).into()),
            ..Default::default()
        })
        .with(Ball);
}

fn despawn_ball(commands: &mut Commands, query: Query<Entity, With<Ball>>) {
    for entity in query.iter() {
        commands.despawn_recursive(entity);
    }
}
