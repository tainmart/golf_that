use crate::consts::*;
use bevy::{app::AppExit, prelude::*};

pub struct MenuPlugin;

impl Plugin for MenuPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app.add_system(button_system.system())
            .add_system(button_press_system.system())
            .init_resource::<ButtonMaterials>()
            .on_state_enter(STAGE, AppState::MainMenu, spawn_main_menu.system())
            .on_state_exit(STAGE, AppState::MainMenu, despawn_main_menu.system());
    }
}

struct MainMenuUI;

struct ButtonMaterials {
    normal: Handle<ColorMaterial>,
    hovered: Handle<ColorMaterial>,
    pressed: Handle<ColorMaterial>,
}

enum MainMenuButton {
    Play,
    Editor,
    Exit,
}

impl FromResources for ButtonMaterials {
    fn from_resources(resources: &Resources) -> Self {
        let mut materials = resources.get_mut::<Assets<ColorMaterial>>().unwrap();
        ButtonMaterials {
            normal: materials.add(Color::hex("27567e").unwrap().into()),
            hovered: materials.add(Color::hex("3c84c3").unwrap().into()),
            pressed: materials.add(Color::hex("1f4565").unwrap().into()),
        }
    }
}

fn button_system(
    button_materials: Res<ButtonMaterials>,
    mut interaction_query: Query<
        (&Interaction, &mut Handle<ColorMaterial>),
        (Mutated<Interaction>, With<Button>),
    >,
) {
    for (interaction, mut material) in interaction_query.iter_mut() {
        match *interaction {
            Interaction::Clicked => {
                *material = button_materials.pressed.clone();
            }
            Interaction::Hovered => {
                *material = button_materials.hovered.clone();
            }
            Interaction::None => {
                *material = button_materials.normal.clone();
            }
        }
    }
}

fn button_press_system(
    query: Query<(&Interaction, &MainMenuButton), (Mutated<Interaction>, With<Button>)>,
    mut state: ResMut<State<AppState>>,
    mut app_exit_events: ResMut<Events<AppExit>>,
) {
    for (interaction, button) in query.iter() {
        if *interaction == Interaction::Clicked {
            match button {
                MainMenuButton::Play => state.set_next(AppState::Ingame).unwrap(),
                MainMenuButton::Editor => {}
                MainMenuButton::Exit => app_exit_events.send(AppExit),
            }
        }
    }
}

fn spawn_main_menu(
    commands: &mut Commands,
    mut materials: ResMut<Assets<ColorMaterial>>,
    asset_server: Res<AssetServer>,
    button_materials: Res<ButtonMaterials>,
) {
    let font: Handle<Font> = asset_server.load("AlbaSuper-dr0g.ttf");
    commands
        // ui camera
        .spawn(CameraUiBundle::default())
        .spawn(ImageBundle {
            style: Style {
                position_type: PositionType::Absolute,
                size: Size::new(Val::Percent(100.0), Val::Percent(100.0)),
                ..Default::default()
            },
            material: materials.add(asset_server.load("background.jpg").into()),
            ..Default::default()
        })
        .with(MainMenuUI)
        // root node
        .spawn(NodeBundle {
            style: Style {
                size: Size::new(Val::Percent(100.0), Val::Percent(100.0)),
                padding: Rect::all(Val::Percent(5.0)),
                flex_direction: FlexDirection::ColumnReverse,
                justify_content: JustifyContent::SpaceAround,
                align_items: AlignItems::Center,
                ..Default::default()
            },
            material: materials.add(Color::NONE.into()),
            ..Default::default()
        })
        .with(MainMenuUI)
        .with_children(|parent| {
            parent
                // Title + Logo
                .spawn(NodeBundle {
                    style: Style {
                        size: Size::new(Val::Percent(75.0), Val::Percent(25.0)),
                        justify_content: JustifyContent::Center,
                        align_items: AlignItems::Center,
                        ..Default::default()
                    },
                    material: materials.add(Color::NONE.into()),
                    ..Default::default()
                })
                .with_children(|parent| {
                    parent
                        // title
                        .spawn(TextBundle {
                            style: Style {
                                margin: Rect::all(Val::Percent(5.0)),
                                ..Default::default()
                            },
                            text: Text {
                                value: "golf_that".to_string(),
                                font: font.clone(),
                                style: TextStyle {
                                    font_size: 150.0,
                                    color: Color::WHITE.into(),
                                    ..Default::default()
                                },
                            },
                            ..Default::default()
                        })
                        // logo
                        .spawn(ImageBundle {
                            style: Style {
                                size: Size::new(Val::Px(250.0), Val::Auto),
                                ..Default::default()
                            },
                            material: materials.add(asset_server.load("logo.png").into()),
                            ..Default::default()
                        });
                })
                // Row of buttons
                .spawn(NodeBundle {
                    style: Style {
                        size: Size::new(Val::Percent(25.0), Val::Percent(50.0)),
                        flex_direction: FlexDirection::ColumnReverse,
                        justify_content: JustifyContent::SpaceEvenly,
                        align_items: AlignItems::Center,
                        ..Default::default()
                    },
                    material: materials.add(Color::NONE.into()),
                    ..Default::default()
                })
                .with_children(|parent| {
                    parent
                        // Play button
                        .spawn(ButtonBundle {
                            style: Style {
                                size: Size::new(Val::Percent(100.0), Val::Percent(30.0)),
                                margin: Rect::all(Val::Auto),
                                justify_content: JustifyContent::Center,
                                align_items: AlignItems::Center,
                                ..Default::default()
                            },
                            material: button_materials.normal.clone(),
                            ..Default::default()
                        })
                        .with(MainMenuButton::Play)
                        .with_children(|parent| {
                            parent.spawn(TextBundle {
                                text: Text {
                                    value: "Play".to_string(),
                                    font: font.clone(),
                                    style: TextStyle {
                                        font_size: 50.0,
                                        color: Color::WHITE,
                                        ..Default::default()
                                    },
                                },
                                ..Default::default()
                            });
                        })
                        // MapEditor button
                        .spawn(ButtonBundle {
                            style: Style {
                                size: Size::new(Val::Percent(100.0), Val::Percent(30.0)),
                                margin: Rect::all(Val::Auto),
                                justify_content: JustifyContent::Center,
                                align_items: AlignItems::Center,
                                ..Default::default()
                            },
                            material: button_materials.normal.clone(),
                            ..Default::default()
                        })
                        .with(MainMenuButton::Editor)
                        .with_children(|parent| {
                            parent.spawn(TextBundle {
                                text: Text {
                                    value: "Map Editor".to_string(),
                                    font: font.clone(),
                                    style: TextStyle {
                                        font_size: 50.0,
                                        color: Color::WHITE,
                                        ..Default::default()
                                    },
                                },
                                ..Default::default()
                            });
                        })
                        // Exit button
                        .spawn(ButtonBundle {
                            style: Style {
                                size: Size::new(Val::Percent(100.0), Val::Percent(30.0)),
                                margin: Rect::all(Val::Auto),
                                justify_content: JustifyContent::Center,
                                align_items: AlignItems::Center,
                                ..Default::default()
                            },
                            material: button_materials.normal.clone(),
                            ..Default::default()
                        })
                        .with(MainMenuButton::Exit)
                        .with_children(|parent| {
                            parent.spawn(TextBundle {
                                text: Text {
                                    value: "Exit".to_string(),
                                    font: font.clone(),
                                    style: TextStyle {
                                        font_size: 50.0,
                                        color: Color::WHITE,
                                        ..Default::default()
                                    },
                                },
                                ..Default::default()
                            });
                        });
                });
        });
}

fn despawn_main_menu(commands: &mut Commands, query: Query<(Entity, &MainMenuUI)>) {
    for (entity, _) in query.iter() {
        commands.despawn_recursive(entity);
    }
}
