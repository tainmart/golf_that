mod ball;
mod camera;
mod consts;
mod menu;
mod utils;

use bevy::{asset::LoadState, prelude::*, render::pass::ClearColor};
use bevy_rapier3d::physics::RapierPhysicsPlugin;
use consts::*;

fn main() {
    App::build()
        .add_resource(ClearColor(Color::rgb(
            0xF9 as f32 / 255.0,
            0xF9 as f32 / 255.0,
            0xF9 as f32 / 255.0,
        )))
        .add_plugins(DefaultPlugins)
        .add_resource(State::new(AppState::AssetLoading))
        .add_stage_after(stage::UPDATE, STAGE, StateStage::<AppState>::default())
        .add_plugin(RapierPhysicsPlugin)
        .add_plugin(menu::MenuPlugin)
        .add_plugin(camera::CameraPlugin)
        .add_plugin(ball::BallPlugin)
        .init_resource::<MapObjectHandles>()
        .on_state_enter(STAGE, AppState::AssetLoading, load_map_objects.system())
        .on_state_update(STAGE, AppState::AssetLoading, check_load_state.system())
        .on_state_enter(STAGE, AppState::Ingame, setup_game.system())
        .run();
}

#[derive(Default)]
struct MapObjectHandles {
    handles: Vec<HandleUntyped>,
}

fn load_map_objects(
    mut map_object_handles: ResMut<MapObjectHandles>,
    asset_server: Res<AssetServer>,
) {
    map_object_handles.handles = asset_server.load_folder("map_objects").unwrap();
}

fn check_load_state(
    mut state: ResMut<State<AppState>>,
    map_object_handles: ResMut<MapObjectHandles>,
    asset_server: Res<AssetServer>,
) {
    if let LoadState::Loaded =
        asset_server.get_group_load_state(map_object_handles.handles.iter().map(|handle| handle.id))
    {
        state.set_next(AppState::MainMenu).unwrap();
    }
}

fn setup_game(
    commands: &mut Commands,
    meshes: ResMut<Assets<Mesh>>,
    materials: ResMut<Assets<StandardMaterial>>,
    asset_server: Res<AssetServer>,
) {
    // light
    commands.spawn(LightBundle {
        transform: Transform::from_translation(Vec3::new(1000.0, 100.0, 2000.0)),
        ..Default::default()
    });

    // Floor
    utils::load_mesh(
        commands,
        asset_server,
        meshes,
        materials,
        "map_objects/Floor.glb#Mesh0/Primitive0",
    );
}
