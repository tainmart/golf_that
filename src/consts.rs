pub const STAGE: &str = "app_state";

#[derive(Clone, PartialEq)]
pub enum AppState {
    AssetLoading,
    MainMenu,
    Ingame,
}
