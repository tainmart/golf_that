import bpy
import os

# get the path where the blend file is located
basedir = bpy.path.abspath("//")
objectdir = os.path.join(basedir, "map_objects")
# check for folder
if not os.path.exists(objectdir):
    os.makedirs(objectdir)

# switch to object mode
bpy.ops.object.mode_set(mode="OBJECT")
# deselect all objects
bpy.ops.object.select_all(action="DESELECT")

scene = bpy.context.scene
for ob in scene.objects:
    bpy.context.view_layer.objects.active = ob
    ob.select_set(state=True)

    ob.location = (0.0, 0.0, 0.0)

    if ob.type == "MESH":
        bpy.ops.export_scene.gltf(filepath=os.path.join(
            objectdir, ob.name + ".glb"), use_selection=True, export_apply=True, export_animations=False)

    ob.select_set(state=False)
